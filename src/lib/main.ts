/**
 * This is a main module for a POC.
 */
export class MainModule {
  /**
   * A value that should be truthy.
   */
  truthy = true;
  /**
   * A value that should be falsy.
   */
  falsy = false;
}
