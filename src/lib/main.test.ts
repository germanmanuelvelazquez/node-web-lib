import { assert } from 'chai';
import 'mocha';
import { MainModule } from './main';

describe('Tests for MainModule', () => {
  it(
    'Truthy and Falsy',
    () => {
      const main = new MainModule();

      assert(main.truthy, '`truthy` must be truthy');
      assert(!main.falsy, '`falsy` must be falsy');
    });
});
